A one-button game made for #TOJam12 -What Twelves Below and adapted for the Hand Eye Society's One-Button Torontron arcade cabinet.


Controls:
- Left Mouse Button to interact
- Escape to quit
- R to restart


Instructions:
- Press the button to interact with the story.