﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Fungus;
public class HoldButton : MonoBehaviour {
    public Flowchart m_flowchart;
    public float currentAlpha;
    public float maxAlpha;
    public float minAlpha;
    public float fillAmount;
    public bool isFilling;
    public bool isOn;
    public GameObject holdImage;
  //  private SpriteRenderer m_spriteRenderer;
    private Image m_Image;
    public string currentBlock;
    public string holdBlock;
    // Use this for initialization
    void Start() {

        m_Image = holdImage.GetComponent<Image>();

    }

    // Update is called once per frame
    void Update()
    {
        if (isOn)
        {

            //if ONE BUTTON held, can fill, else will defill
            if (Input.GetButton("OneButton"))
            {
                isFilling = true;
            }
            else
            {
                isFilling = false;
            }

            if (isFilling)
            {
                currentAlpha += fillAmount;

            }
            else if (!isFilling)
            {
                currentAlpha -= fillAmount;
            }

            #region GivenQuest
            //check if hit min or max
            if (currentAlpha >= maxAlpha)
            {

                //get reference to initiating block
                Block curBlock = m_flowchart.FindBlock("Given Quest");
                if (curBlock)
                {
                    //if initiating block is still active
                    if (curBlock == m_flowchart.SelectedBlock)
                    {

                        //stop all commands just in case
                        curBlock.Stop();
                        //reset bool
                        currentAlpha = 0;
                        //transition to next block
                        m_flowchart.ExecuteBlock("Cup A");
                        m_flowchart.SelectedBlock = m_flowchart.FindBlock("Cup A");


                    }

                }
            }
            #endregion
            #region CupA
            //check if hit min or max
            if (currentAlpha >= maxAlpha)
            {

                //get reference to initiating block
                Block curBlock = m_flowchart.FindBlock("Cup A");
                if (curBlock)
                {
                    //if initiating block is still active
                    if (curBlock == m_flowchart.SelectedBlock)
                    {

                        //stop all commands just in case
                        curBlock.Stop();
                        //reset bool
                        currentAlpha = 0;
                        //transition to next block
                        m_flowchart.ExecuteBlock("Cup B");
                        m_flowchart.SelectedBlock = m_flowchart.FindBlock("Cup B");


                    }

                }
            }
            #endregion
            #region CupB
            //check if hit min or max
            if (currentAlpha >= maxAlpha)
            {

                //get reference to initiating block
                Block curBlock = m_flowchart.FindBlock("Cup B");
                if (curBlock)
                {
                    //if initiating block is still active
                    if (curBlock == m_flowchart.SelectedBlock)
                    {

                        //stop all commands just in case
                        curBlock.Stop();
                        //reset bool
                        currentAlpha = 0;
                        //transition to next block
                        m_flowchart.ExecuteBlock("Cup C");
                        m_flowchart.SelectedBlock = m_flowchart.FindBlock("Cup C");


                    }

                }
            }
            #endregion

            if (currentAlpha < minAlpha)
            {
                currentAlpha = minAlpha;
            }

            //update color
            m_Image.color = new Color(m_Image.color.r, m_Image.color.g, m_Image.color.b, currentAlpha);

        }
    }


    public void TurnOnHold()
    {
        isOn = true;
 
            currentBlock = "Given Quest";
            m_flowchart.SelectedBlock = m_flowchart.FindBlock(currentBlock);

        

    }


    //disable hold interaction
    public void TurnOffHold()
    {
        isOn = false;
        m_Image.color = new Color(m_Image.color.r, m_Image.color.g, m_Image.color.b, 0);

    }


}
