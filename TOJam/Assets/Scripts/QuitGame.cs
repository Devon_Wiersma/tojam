﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Fungus;

public class QuitGame : MonoBehaviour {

    public Flowchart m_flowchart;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

      //  Debug.Log(m_flowchart.SelectedBlock.BlockName);

        if (Input.GetButtonDown("QuitGame"))
        {
            Application.Quit();
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
        }

	}
}
