﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class TimedInterrupt : MonoBehaviour
{
    public Writer writerScript;
    public Flowchart m_flowchart;
    public string potentialPath = "blank";
    public string currentBlock;
    public bool isOn;
    private float originalSpeed, modifiedSpeed = 27;


    private void Start()
    {
        originalSpeed = writerScript.writingSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        if (isOn)
        {
            if (Input.GetButtonDown("OneButton"))
            {
                switch (potentialPath)
                {
                    case "hellPath":
                        //if button pressed, go down hell path
                        if (m_flowchart.SelectedBlock.BlockName == "Enter Cave")
                        {
                            Debug.Log("Hell Path A enabled");

                            currentBlock = "Hell Path A";
                            potentialPath = "hellPathB";
                            m_flowchart.SelectedBlock.Stop();
                            m_flowchart.ExecuteBlock("Hell Path A");
                            m_flowchart.SelectedBlock = m_flowchart.FindBlock("Hell Path A");
                        }
                        break;
                    case "goblinPath":
                        //if button pressed, go down goblin path
                        if (m_flowchart.SelectedBlock.BlockName == "Enter Cave")
                        {
                            m_flowchart.SelectedBlock.Stop();
                            m_flowchart.ExecuteBlock("Goblin Path A");
                            m_flowchart.SelectedBlock = m_flowchart.FindBlock("Goblin Path A");
                        }
                        break;
                    case "hellPathB":
                        //if button pressed, go to hell path b
                        if (m_flowchart.SelectedBlock.BlockName == "Hell Path A")
                        {
                            Debug.Log("Hell Path B enabled");

                            currentBlock = "Hell Path B";

                            potentialPath = "hellPathC";
                            m_flowchart.SelectedBlock.Stop();
                            m_flowchart.ExecuteBlock("Hell Path B");
                            m_flowchart.SelectedBlock = m_flowchart.FindBlock("Hell Path B");
                        }
                        break;
                    case "hellPathC":
                        //if button pressed, go to hell path c
                        if (m_flowchart.SelectedBlock.BlockName == "Hell Path B")
                        {
                            Debug.Log("Hell Path C enabled");

                            currentBlock = "Hell Path C";

                            m_flowchart.SelectedBlock.Stop();
                            m_flowchart.ExecuteBlock("Hell Path C");
                            m_flowchart.SelectedBlock = m_flowchart.FindBlock("Hell Path C");
                        }
                        break;
                }

            }
        }
    }

    public void TurnOnInterrupt()
    {
        isOn = true;
        if (potentialPath == "blank")
        {
            potentialPath = "hellPath";
            currentBlock = "Enter Cave";
            m_flowchart.SelectedBlock = m_flowchart.FindBlock(currentBlock);
            Debug.Log("Hell Path enabled");

        }

    }

    public void TurnOffInterrupt()
    {
        Debug.Log("Choice Disabled");
        m_flowchart.SelectedBlock = m_flowchart.FindBlock(currentBlock);
        isOn = false;
    }



    public void ChangeToGoblinPath()
    {
        potentialPath = "goblinPath";
        Debug.Log("Goblin Path enabled");
        m_flowchart.SelectedBlock = m_flowchart.FindBlock(currentBlock);



    }

    public void ResetPaths()
    {
        writerScript.writingSpeed = originalSpeed;
        potentialPath = "blank";
        currentBlock = "Enter Cave";
    }

    public void ModifyWritingSpeed()
    {
        writerScript.writingSpeed = modifiedSpeed;

    }

    public void ResetWritingSpeed()
    {
        writerScript.writingSpeed = originalSpeed;

    }
}
