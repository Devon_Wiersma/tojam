﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class InterruptButton : MonoBehaviour {

    public Flowchart m_flowchart;
    public ContinueButton continueScript;
    public bool isOn;
    public string currentBlock;
    public string interruptBlock;
    public List<Command> commandList = new List<Command>();

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (isOn)
        {
            #region EnterCave
            if (Input.GetButtonDown("OneButton"))
            {
                if (continueScript.canContinue == false)
                {
                    Block curBlock = m_flowchart.SelectedBlock;
                    if (curBlock)
                    {
                        if (curBlock.BlockName == "Hell Path A")
                        {

                            if (curBlock.IsExecuting() == true)
                            {

                                commandList = curBlock.CommandList;
                                if (curBlock.ActiveCommand == commandList[5] || curBlock.ActiveCommand == commandList[6])
                                {
                                    curBlock.Stop();
                                    m_flowchart.ExecuteBlock("Hell Path B");
                                    m_flowchart.SelectedBlock = m_flowchart.FindBlock("Hell Path B");
                                    Debug.Log("I ran");

                                }

                            }

                        } else if (curBlock.BlockName == "Hell Path B")
                        {

                            if (curBlock.IsExecuting() == true)
                            {

                                commandList = curBlock.CommandList;
                                if (curBlock.ActiveCommand == commandList[6] || curBlock.ActiveCommand == commandList[7])
                                {
                                    curBlock.Stop();
                                    m_flowchart.ExecuteBlock("Hell Path C");
                                    m_flowchart.SelectedBlock = m_flowchart.FindBlock("Hell Path C");


                                }

                            }
                        }
                    }
                    else
                    {
                        return;
                    }
                }


            }
            #endregion

       
        }
    }

    public void TurnOn()
    {
        isOn = true;
    }

    public void TurnOff()
    {
        isOn = false;
    }
}
