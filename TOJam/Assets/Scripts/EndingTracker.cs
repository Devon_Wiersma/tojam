﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EndingTracker : MonoBehaviour {

    public Text menuText;
    public Text creditsText;
    public int numOfEndingsReached;
    public bool[] endingReached;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void AddEnding(int endingNum)
    {
        endingReached[endingNum] = true;
    }

    public void SumEndings()
    {
        numOfEndingsReached = 0;

        for (int i = 0; i < endingReached.Length; i++)
        {
            if (endingReached[i] == true)
            {
                numOfEndingsReached++;
            }
        }

        if (numOfEndingsReached < 9)
        {
            menuText.text = numOfEndingsReached + "/9 endings reached!";
            creditsText.text = numOfEndingsReached + "/9 endings achieved. Play again to find more!";
        } else if (numOfEndingsReached == 9)
        {
            menuText.text = numOfEndingsReached + "All endings found!";
            creditsText.text = numOfEndingsReached + "All endings found!";
        }
    }

    public void ResetTracker()
    {
        numOfEndingsReached = 0;
        for (int i = 0; i < endingReached.Length; i++)
        {
            endingReached[i] = false;
        }
    }
}
