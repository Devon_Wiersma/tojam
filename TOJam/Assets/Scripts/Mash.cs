﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using UnityEngine.UI;

public class Mash : MonoBehaviour {


    public Text debugText;
    public Flowchart flowchart;
    public ContinueButton continueScript;
    [Tooltip("time that press is recognized in seconds")]
    public float lifeOfPress;
    public string currentBlockName;
    int presses,interupts = 0;

 public   bool active;
	void Update () {

        if (active)
        {
            if (Input.GetButtonDown("OneButton"))
            {
                if (continueScript.canContinue == false)
                {
                    StartCoroutine(Press());
                    flowchart.SendFungusMessage("Jiggle");
                }
            }

            if (presses >= 5)
            {
                Block enterTavernBlock = flowchart.FindBlock("Enter Tavern");
                if (enterTavernBlock)
                {
                    enterTavernBlock.Stop();
                }
                debugText.text = flowchart.SelectedBlock.BlockName;

                flowchart.SendFungusMessage("Mash" + interupts.ToString());
                if(interupts == 0)
                {
                    currentBlockName = "Confirmation";
                    flowchart.SelectedBlock = flowchart.FindBlock("Confirmation");
                } else if (interupts == 1)
                {
                    flowchart.SelectedBlock = flowchart.FindBlock("Condensed Story");
                    interupts = 0;
                }
                presses = 0;
                StopAllCoroutines();
                interupts++;
            }
        }

	}
    
    public void ToggleActive()
    {
        active = !active;
        if(flowchart.SelectedBlock.BlockName == "Enter Tavern")
        {
            currentBlockName = "Enter Tavern";
        }
        if(flowchart.SelectedBlock.BlockName == "Given Quest")
        {
            active = false;
        }
    }

    public void TurnOff()
    {
        interupts = 0;
        presses = 0;
        active = false;
    }

    IEnumerator Press()
    {
        presses += 1;

        yield return new WaitForSeconds(lifeOfPress);

        presses -= 1;
        

    }

    public void SetSelectedBlock()
    {
        flowchart.SelectedBlock = flowchart.FindBlock(currentBlockName);
    }

}
