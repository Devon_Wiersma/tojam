﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using Fungus;

public class Menu : MonoBehaviour {

   public Flowchart m_flowchart;

public void ReturnToMenu()
    {
        m_flowchart.SelectedBlock = m_flowchart.FindBlock("Main Menu");
    }

}
