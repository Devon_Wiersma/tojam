﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveUI : MonoBehaviour
{

    public RectTransform flowPage, leftPage, rightPage, lowerHalf, center;
    public RectTransform flowPageText, leftPageText, rightPageText, lowerHalfText, centerText;
    public AnimationCurve myCurve;
    public float lerpDuration;



    public void MoveToLeft()
    {
        StopAllCoroutines();
        StartCoroutine(LerpUI(leftPage, leftPageText));
    }
    public void MoveToRight()
    {
        StopAllCoroutines();
        StartCoroutine(LerpUI(rightPage, rightPageText));
    }
    public void MoveToLower()
    {
        StopAllCoroutines();
        StartCoroutine(LerpUI(lowerHalf, lowerHalfText));
    }
    public void MoveToCenter()
    {
        StopAllCoroutines();
        StartCoroutine(LerpUI(center, centerText));
    }



    IEnumerator LerpUI(RectTransform destination,RectTransform destinText)
    {

        float timer = 0f;
        RectTransform lastPosition = flowPage,
            flowTextLast = flowPageText;

        while (timer < lerpDuration)
        {
            timer += Time.deltaTime;
            Debug.Log(flowPage.anchorMin + " / " + destination.anchorMin);
            //flowPage.position = Vector3.Lerp(lastPosition.position, destination.position, myCurve.Evaluate(timer / lerpDuration));
            // flowPage.localScale = Vector3.Lerp(lastPosition.localScale, destination.localScale, myCurve.Evaluate(timer / lerpDuration));
            flowPage.offsetMin = Vector2.Lerp(lastPosition.offsetMin, destination.offsetMin, myCurve.Evaluate(timer / lerpDuration));
            flowPage.offsetMax = Vector2.Lerp(lastPosition.offsetMax, destination.offsetMax, myCurve.Evaluate(timer / lerpDuration));
            flowPageText.offsetMin = Vector2.Lerp(flowTextLast.offsetMin, destinText.offsetMin, myCurve.Evaluate(timer / lerpDuration));
            flowPageText.offsetMax = Vector2.Lerp(flowTextLast.offsetMax, destinText.offsetMax, myCurve.Evaluate(timer / lerpDuration));

            yield return 0f;
        }
        flowPage.offsetMin = destination.offsetMin;
        flowPage.offsetMax = destination.offsetMax;
        flowPageText.offsetMin = destinText.offsetMin;
        flowPageText.offsetMax = destinText.offsetMax;
    }


}
