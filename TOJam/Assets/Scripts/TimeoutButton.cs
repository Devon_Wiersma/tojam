﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Fungus;

public class TimeoutButton : MonoBehaviour
{
    public Text debugText;

    //use this script to transition to a block based on a timeout

    public Flowchart m_flowchart;
    public ContinueButton continueScript;

    //set the amount of seconds to wait before executing after the dialogue has finished
    public float confirmationTime,killDragonTime, standoffATime, standoffBTime;
    public string currentBlock;
    public string timeoutBlock;

    //call this through an invoke from the block to start the timer
    public void StartTimer()
    {
        StopAllCoroutines();

        Block curBlock = m_flowchart.SelectedBlock;
        Block nextBlock = m_flowchart.SelectedBlock;
        float timerValue = 0;

        if (curBlock.BlockName == "Confirmation" || curBlock.BlockName == "ShakingTavern")
        {
            timerValue = confirmationTime;
            curBlock = m_flowchart.FindBlock("Confirmation");
            nextBlock = m_flowchart.FindBlock("Given Quest");
        }
        if(curBlock.BlockName == "Find & Kill Dragon")
        {
            timerValue = killDragonTime;
            nextBlock = m_flowchart.FindBlock("Dragon Standoff A");

        }
        if (curBlock.BlockName == "Dragon Standoff A")
        {
            timerValue = standoffATime;
            nextBlock = m_flowchart.FindBlock("Dragon Standoff B");

        }
        if (curBlock.BlockName == "Dragon Standoff B")
        {
            timerValue = standoffBTime;
            nextBlock = m_flowchart.FindBlock("Dragon Standoff C");

        }


        StartCoroutine(TimerCountdown( curBlock, nextBlock, timerValue));
    }



    //if timer completes and the initiating block is still active (ie has not transitioned via a different method, execute timeout block
    IEnumerator TimerCountdown(Block fromBlock, Block toBlock, float timer)
    {
        yield return new WaitForSeconds(timer);

            if (fromBlock == m_flowchart.SelectedBlock)
            {
                if (fromBlock.ActiveCommand == null)
                {
                    if(fromBlock.BlockName == "Confirmation")
                    {
                        m_flowchart.SetBooleanVariable("alternateEnding", true);
                    }
                    fromBlock.Stop();
                debugText.text = m_flowchart.SelectedBlock.BlockName;

                m_flowchart.ExecuteBlock(toBlock);
                m_flowchart.SelectedBlock = toBlock;
                continueScript.canContinue = false;

            }

        } else if (m_flowchart.SelectedBlock.BlockName == "ShakingTavern")
        {

                m_flowchart.SetBooleanVariable("alternateEnding", true);
            fromBlock.Stop();
            debugText.text = m_flowchart.SelectedBlock.BlockName;

            m_flowchart.ExecuteBlock("Given Quest");
            m_flowchart.SelectedBlock = m_flowchart.FindBlock("Given Quest");

        }


    }

    public void StopTimer()
    {
        StopAllCoroutines();
    }


}
