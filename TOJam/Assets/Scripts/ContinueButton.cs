﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Fungus;

public class ContinueButton : MonoBehaviour {

    //use this for standard transitions (continuing after dialogue completed)

    public Text debugText;
    public Flowchart m_flowchart;
    public bool canContinue = false;
    public string currentBlock;
    public string continueBlock;
    public string[] endingBlocks;

    // Update is called once per frame
    void Update()
    {
        Debug.Log( m_flowchart.SelectedBlock.BlockName);


        //check if ONE BUTTON has been pressed
        if (Input.GetButtonDown("OneButton"))
        {
            #region Menu
            //has the dialogue finished?
            if (canContinue)
            {
                //get reference to initiating block
                Block curBlock = m_flowchart.FindBlock("Main Menu");
                if (curBlock)
                {
                    //if initiating block is still active
                    if (curBlock == m_flowchart.SelectedBlock || m_flowchart.SelectedBlock.BlockName == "ShakingTavern")
                    {
                        //check no commands are active
                        if (curBlock.ActiveCommand == null)
                        {
                            //stop all commands just in case
                            curBlock.Stop();
                            //transition to next block
                            m_flowchart.ExecuteBlock("Enter Tavern");
                            m_flowchart.SelectedBlock = m_flowchart.FindBlock("Enter Tavern");

                            //reset bool
                            canContinue = false;
                        }
                    }
                }
            }
            #endregion


            #region EnterTavern
            //has the dialogue finished?
            if (canContinue)
            {
                //get reference to initiating block
                Block curBlock = m_flowchart.FindBlock("Enter Tavern");
                if (curBlock)
                {
                    //if initiating block is still active
                    if (curBlock == m_flowchart.SelectedBlock || m_flowchart.SelectedBlock.BlockName == "ShakingTavern")
                    {
                 
                            //stop all commands just in case
                            curBlock.Stop();
                            debugText.text = m_flowchart.SelectedBlock.BlockName;
                            //transition to next block
                            m_flowchart.ExecuteBlock("Given Quest");
                            m_flowchart.SelectedBlock = m_flowchart.FindBlock("Given Quest");

                            //reset bool
                            canContinue = false;
                        
                    }
                }
            }
            #endregion
            #region GivenQuest
            if (canContinue)
            {
                //get reference to initiating block
                Block curBlock = m_flowchart.FindBlock("Given Quest");
                if (curBlock)
                {
                    //if initiating block is still active
                    if (curBlock == m_flowchart.SelectedBlock)
                    {
                     
                            //stop all commands just in case
                            curBlock.Stop();
                            //transition to next block
                            m_flowchart.ExecuteBlock("Enter Cave");
                            m_flowchart.SelectedBlock = m_flowchart.FindBlock("Enter Cave");

                            //reset bool
                            canContinue = false;
                        
                    }
                }
            }
            #endregion
            #region CupA
            if (canContinue)
            {
                //get reference to initiating block
                Block curBlock = m_flowchart.FindBlock("Cup A");
                if (curBlock)
                {
                    //if initiating block is still active
                    if (curBlock == m_flowchart.SelectedBlock)
                    {
                  
                            //stop all commands just in case
                            curBlock.Stop();
                            //transition to next block
                            m_flowchart.ExecuteBlock("Enter Cave");
                            m_flowchart.SelectedBlock = m_flowchart.FindBlock("Enter Cave");

                            //reset bool
                            canContinue = false;
                        
                    }
                }
            }
            #endregion
            #region CupB
            if (canContinue)
            {
                //get reference to initiating block
                Block curBlock = m_flowchart.FindBlock("Cup B");
                if (curBlock)
                {
                    //if initiating block is still active
                    if (curBlock == m_flowchart.SelectedBlock)
                    {
                    
                            //stop all commands just in case
                            curBlock.Stop();
                            //transition to next block
                            m_flowchart.ExecuteBlock("Enter Cave");
                            m_flowchart.SelectedBlock = m_flowchart.FindBlock("Enter Cave");

                            //reset bool
                            canContinue = false;
                        
                    }
                }
            }
            #endregion


            #region EnterCave
            if (canContinue)
            {
                //get reference to initiating block
                Block curBlock = m_flowchart.FindBlock("Enter Cave");
                if (curBlock)
                {
                    //if initiating block is still active
                    if (curBlock == m_flowchart.SelectedBlock)
                    {
                    
                            //stop all commands just in case
                            curBlock.Stop();
                            //transition to next block
                            m_flowchart.ExecuteBlock("Find & Kill Dragon");
                            m_flowchart.SelectedBlock = m_flowchart.FindBlock("Find & Kill Dragon");

                            //reset bool
                            canContinue = false;
                        
                    }
                }
            }
            #endregion
            #region HellPathA
            if (canContinue)
            {
                //get reference to initiating block
                Block curBlock = m_flowchart.FindBlock("Hell Path A");
                if (curBlock)
                {
                    //if initiating block is still active
                    if (curBlock == m_flowchart.SelectedBlock)
                    {
                      
                            //stop all commands just in case
                            curBlock.Stop();
                            //transition to next block
                            m_flowchart.ExecuteBlock("Find & Kill Dragon");
                            m_flowchart.SelectedBlock = m_flowchart.FindBlock("Find & Kill Dragon");

                            //reset bool
                            canContinue = false;
                        
                    }
                }
            }
            #endregion
            #region HellPathB
            if (canContinue)
            {
                //get reference to initiating block
                Block curBlock = m_flowchart.FindBlock("Hell Path B");
                if (curBlock)
                {
                    //if initiating block is still active
                    if (curBlock == m_flowchart.SelectedBlock)
                    {
                   
                            //stop all commands just in case
                            curBlock.Stop();
                            //transition to next block
                            m_flowchart.ExecuteBlock("Find & Kill Dragon");
                            m_flowchart.SelectedBlock = m_flowchart.FindBlock("Find & Kill Dragon");

                            //reset bool
                            canContinue = false;
                        
                    }
                }
            }
            #endregion


            #region GoblinPathA
            if (canContinue)
            {
                //get reference to initiating block
                Block curBlock = m_flowchart.FindBlock("Goblin Path A");
                if (curBlock)
                {
                    //if initiating block is still active
                    if (curBlock == m_flowchart.SelectedBlock)
                    {
                  
                            //stop all commands just in case
                            curBlock.Stop();
                            //transition to next block
                            m_flowchart.ExecuteBlock("Goblin Path B");
                            m_flowchart.SelectedBlock = m_flowchart.FindBlock("Goblin Path B");

                            //reset bool
                            canContinue = false;
                        
                    }
                }
            }
            #endregion
            #region GoblinPathB
            if (canContinue)
            {
                //get reference to initiating block
                Block curBlock = m_flowchart.FindBlock("Goblin Path B");
                if (curBlock)
                {
                    //if initiating block is still active
                    if (curBlock == m_flowchart.SelectedBlock)
                    {
                    
                            //stop all commands just in case
                            curBlock.Stop();
                            //transition to next block
                            m_flowchart.ExecuteBlock("Goblin Path C");
                            m_flowchart.SelectedBlock = m_flowchart.FindBlock("Goblin Path C");

                            //reset bool
                            canContinue = false;
                        
                    }
                }
            }
            #endregion


            //cont button path determined by alternate ending trigger
            #region FindAndKillDragon
            if (canContinue)
            {
                //get reference to initiating block
                Block curBlock = m_flowchart.FindBlock("Find & Kill Dragon");
                if (curBlock)
                {
                    //if initiating block is still active
                    if (curBlock == m_flowchart.SelectedBlock)
                    {
                      
                            if (m_flowchart.GetBooleanVariable("alternateEnding") == true)
                            {
                                //stop all commands just in case
                                curBlock.Stop();
                                //transition to next block
                                m_flowchart.ExecuteBlock("Alt End");
                                m_flowchart.SelectedBlock = m_flowchart.FindBlock("Alt End");

                                //reset bool
                                canContinue = false;
                            } else if (m_flowchart.GetBooleanVariable("alternateEnding") == false)
                            {
                                //stop all commands just in case
                                curBlock.Stop();
                                //transition to next block
                                m_flowchart.ExecuteBlock("Rewarded By King Main");
                                m_flowchart.SelectedBlock = m_flowchart.FindBlock("Rewarded By King Main");

                                //reset bool
                                canContinue = false;
                            }
                        
                    }
                }
            }
            #endregion
            #region DragonStandoffA
            if (canContinue)
            {
                //get reference to initiating block
                Block curBlock = m_flowchart.FindBlock("Dragon Standoff A");
                if (curBlock)
                {
                    //if initiating block is still active
                    if (curBlock == m_flowchart.SelectedBlock)
                    {
                      
                            if (m_flowchart.GetBooleanVariable("alternateEnding") == true)
                            {
                                //stop all commands just in case
                                curBlock.Stop();
                                //transition to next block
                                m_flowchart.ExecuteBlock("Alt End");
                                m_flowchart.SelectedBlock = m_flowchart.FindBlock("Alt End");

                                //reset bool
                                canContinue = false;
                            }
                            else if (m_flowchart.GetBooleanVariable("alternateEnding") == false)
                            {
                                //stop all commands just in case
                                curBlock.Stop();
                                //transition to next block
                                m_flowchart.ExecuteBlock("Rewarded By King Main");
                                m_flowchart.SelectedBlock = m_flowchart.FindBlock("Rewarded By King Main");

                                //reset bool
                                canContinue = false;
                            }
                        
                    }
                }
            }
            #endregion
            #region DragonStandoffB
            if (canContinue)
            {
                //get reference to initiating block
                Block curBlock = m_flowchart.FindBlock("Dragon Standoff B");
                if (curBlock)
                {
                    //if initiating block is still active
                    if (curBlock == m_flowchart.SelectedBlock)
                    {
                        
                            if (m_flowchart.GetBooleanVariable("alternateEnding") == true)
                            {
                                //stop all commands just in case
                                curBlock.Stop();
                                //transition to next block
                                m_flowchart.ExecuteBlock("Alt End");
                                m_flowchart.SelectedBlock = m_flowchart.FindBlock("Alt End");

                                //reset bool
                                canContinue = false;
                            }
                            else if (m_flowchart.GetBooleanVariable("alternateEnding") == false)
                            {
                                //stop all commands just in case
                                curBlock.Stop();
                                //transition to next block
                                m_flowchart.ExecuteBlock("Rewarded By King Main");
                                m_flowchart.SelectedBlock = m_flowchart.FindBlock("Rewarded By King Main");

                                //reset bool
                                canContinue = false;
                            }
                        
                    }
                }
            }
            #endregion

            #region AltEnd
            if (canContinue)
            {
                //get reference to initiating block
                Block curBlock = m_flowchart.FindBlock("Alt End");
                if (curBlock)
                {
                    //if initiating block is still active
                    if (curBlock == m_flowchart.SelectedBlock)
                    {
                        m_flowchart.SetBooleanVariable("enableChoice", true);

                    }
                }
            }
            #endregion


            //checks all viable blocks to transition to the end credits
            #region ToEndCredits
            if (canContinue)
            {
                for (int i = 0; i < endingBlocks.Length; i++) {
                        //if initiating block is still active
                        if (m_flowchart.SelectedBlock.BlockName == endingBlocks[i]) { 
                           
                                //stop all commands just in case
                                m_flowchart.SelectedBlock.Stop();

                            //transition to next block
                            m_flowchart.ExecuteBlock("END");
                            m_flowchart.SelectedBlock = m_flowchart.FindBlock("END");

                            //reset bool
                            canContinue = false;
                            
                    }
                }
            }

            #endregion

            #region END
            //has the dialogue finished?
            if (canContinue)
            {
                //get reference to initiating block
                Block curBlock = m_flowchart.FindBlock("END");
                if (curBlock)
                {
                    //if initiating block is still active
                    if (curBlock == m_flowchart.SelectedBlock || m_flowchart.SelectedBlock.BlockName == "ShakingTavern")
                    {
                     
                            //stop all commands just in case
                            curBlock.Stop();
                            //transition to next block
                            m_flowchart.ExecuteBlock("Load Menu");
                            m_flowchart.SelectedBlock = m_flowchart.FindBlock("Load Menu");

                            //reset bool
                            canContinue = false;
                        
                    }
                }
            }
            #endregion


        }

    }

  public void EnableContinue()
    {

        canContinue = true;
    }
}
