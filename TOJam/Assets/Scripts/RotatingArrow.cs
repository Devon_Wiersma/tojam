﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;


public class RotatingArrow : MonoBehaviour
{
    public Flowchart flowchart;
    public AnimationCurve animCurve, shotCurve;
    public Transform arrowTarget;
    public float rotationSpeed, lerpTime;
    public Animator leftChoice, rightChoice;
    float tempRotSpeed, finalAngle;
    bool desicionMade, regicide, startMoving;

    void Start()
    {
        tempRotSpeed = rotationSpeed;
    }

    void Update()
    {
        if (startMoving)
        {
            if (!desicionMade)
                RotateBackAndForth();
            if (regicide)
            {

            }
            if (flowchart.SelectedBlock.BlockName == "Alt End")
            {
                if (Input.GetButtonDown("OneButton"))
                {
                    Decide();
                    Debug.Log("Has Decided");
                }
            }
        }
    }
    public void ToggleTheDecidining()
    {
        startMoving = !startMoving;
    }

   public void ResetVariables()
    {
        tempRotSpeed = rotationSpeed;
        finalAngle = 0;
        desicionMade = false;
        regicide = false;
        startMoving = false;
    }


    void RotateBackAndForth()
    {
        transform.Rotate(Vector3.forward, Time.deltaTime * tempRotSpeed);


        if (tempRotSpeed > 0 && transform.rotation.eulerAngles.z >= 90f && transform.rotation.eulerAngles.z < 180)
        {
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, 90));
            tempRotSpeed = 0;
            StartCoroutine(Pause(-1));
        }

        if (tempRotSpeed < 0 && transform.rotation.eulerAngles.z <= 270f && transform.rotation.eulerAngles.z > 180)
        {
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, 270));
            tempRotSpeed = 0;
            StartCoroutine(Pause(1));
        }
    }
    void Decide()
    {
        desicionMade = true;
        //Left Choice
        if (transform.rotation.eulerAngles.z >= 31f && transform.rotation.eulerAngles.z < 180)
        {
            finalAngle = 90;
            StartCoroutine(RotateToPoint());
        }
        //Right Choice
        if (transform.rotation.eulerAngles.z <= 330f && transform.rotation.eulerAngles.z > 180)
        {
            finalAngle = 270;
            StartCoroutine(RotateToPoint());
        }
        //Center Choice
        if (transform.rotation.eulerAngles.z <= 30f || transform.rotation.eulerAngles.z >= 331f)
        {
            finalAngle = 0.1f;
            StartCoroutine(RotateToPoint());
        }
    }
    void ChoiceMade()
    {

        if (finalAngle == 90)
        {
            leftChoice.SetTrigger("chosen");
            flowchart.SelectedBlock.Stop();

            flowchart.SendFungusMessage("Left");
            flowchart.SelectedBlock = flowchart.FindBlock("Fame End");

        }
        else if (finalAngle == 270)
        {
            rightChoice.SetTrigger("chosen");
            flowchart.SelectedBlock.Stop();

            flowchart.SendFungusMessage("Right");
            flowchart.SelectedBlock = flowchart.FindBlock("Money End");

        }
        else
        {
            regicide = true;
            flowchart.SelectedBlock.Stop();

            flowchart.SendFungusMessage("KillKing");
            flowchart.SelectedBlock = flowchart.FindBlock("King Kill End");

        }
        ResetVariables();
    }


    IEnumerator RotateToPoint()
    {
        float timer = 0f;
        Quaternion start = transform.rotation, end = Quaternion.Euler(0, 0, finalAngle);

        while (timer < lerpTime)
        {
            Debug.Log(timer);
            timer += Time.deltaTime;
            if (finalAngle == .1f)
                transform.rotation = Quaternion.Lerp(start, end, animCurve.Evaluate(timer / lerpTime));


            yield return 0f;
        }
        if (finalAngle == .1f)
        {
            StartCoroutine(ShootArrow());
        }
        ChoiceMade();

    }
    IEnumerator ShootArrow()
    {
        float timer = 0f;
        Vector3 start = transform.position, end = arrowTarget.position;

        while (timer < lerpTime)
        {
            Debug.Log(timer);
            timer += Time.deltaTime;
            transform.position = Vector3.Lerp(start, end, shotCurve.Evaluate(timer / lerpTime));

            yield return 0f;
        }
        Invoke("CommitRegicide", 2);
    }
    void CommitRegicide()
    {
        flowchart.SendFungusMessage("regicide");
    }

    IEnumerator Pause(float orient)
    {
        yield return new WaitForSeconds(2f);
        tempRotSpeed = rotationSpeed * orient;
    }

}
